var gulp = require("gulp"),
    runsequence = require("run-sequence"),
    del = require("del"),
    pump = require("pump"),
    gulpif = require("gulp-if"),
    gutil = require("gulp-util"),
    concat = require("gulp-concat"),
    rename = require("gulp-rename"),
    browserify = require("gulp-browserify"),
    uglify = require("gulp-uglify"),
    sass = require("gulp-sass"),
    cssnano = require("gulp-cssnano"),
    htmlmin = require("gulp-htmlmin"),
    htmlreplace = require("gulp-html-replace");

var fs = require('fs'),
    pkgJson = JSON.parse(fs.readFileSync('./package.json'));

var env,
    jsSources,
    sassSources,
    htmlSources,
    outputDir,
    minify;

env = process.env.NODE_ENV || "local";

if (env === "local") {
    outputDir = "src/www";
    minify = false;
} else {
    outputDir = "dist/www";
    minify = true;
}

jsSources = [
    "src/components/js/main.js",
    "src/components/js/menu.js"
];

sassSources = [
    "src/components/sass/style.scss"
];

htmlSources = [
    "src/www/**/*.html"
];

gulp.task("build:js", function(callback) {
    pump([
        gulp.src(jsSources),
        concat("script.js"),
        browserify(),
        gulpif(minify, uglify()),
        gulpif(minify, rename("bundle.min.js"), rename("bundle.js")),
        gulp.dest(outputDir + "/js")
    ],
        callback
    );
});

gulp.task("build:sass", function(callback) {
    pump([
        gulp.src(sassSources),
        sass(),
        gulpif(minify, cssnano()),
        gulpif(minify, rename("style.min.css"), rename("style.css")),
        gulp.dest(outputDir + "/css")
    ],
        callback
    );
});

gulp.task("build:html", function(callback) {
    pump([
        gulp.src(htmlSources),
        gulpif(minify, htmlreplace({
            "css": "style.min.css?v" + pkgJson.version,
            "js": "bundle.min.js?v" + pkgJson.version
        })),
        gulpif(minify, htmlmin({
            collapseWhitespace: true
        })),
        gulp.dest(outputDir)
    ],
        callback
    );
});

gulp.task("watch", function() {
    gulp.watch(jsSources, ["build:js"]);
    gulp.watch("src/components/sass/*.scss", ["build:sass"]);
});

gulp.task("log:env", function() {
    gutil.log("NODE_ENV: ", env);
});

gulp.task("clean:dist", function(){
    return del(["dist/**/*"]);
})

gulp.task("build", function(callback) {
    runsequence("clean:dist", ["log:env", "build:js", "build:sass", "build:html"], callback)    
});

gulp.task("default", ["build", "watch"]);